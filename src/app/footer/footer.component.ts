import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private _router: Router, private _current_route: ActivatedRoute) { }

  ngOnInit() {
  }

  scrollToElement(element,current_route) {  //this code is to navigate to a portin on page with smooth scrolling effect
    var parent = document.getElementsByClassName('.parent-route');
    if (this._router.url === current_route) {
      var position = $('#' + element).offset().top - 50;
      console.log(position);
      $('html,body').animate({ scrollTop: position + "px" }, 800);
    }
    else {
      window.scrollTo(0,0);
      this._router.navigate([current_route]);  
    }
  }
  

}
