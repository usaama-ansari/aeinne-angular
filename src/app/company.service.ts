import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class CompanyService {

  constructor(private _http: Http) { }

  checkCompliance(companyname: string) {
    console.log(companyname);
    //return this._http.get('http://34.198.21.113:3000/api/checkcompliance/' + companyname)
    return this._http.get('http://localhost:3000/api/checkcompliance/' + companyname)
      .map((response: Response) => response.json());
  }

  getSuggestions(string: string) {
    //return this._http.get('http://34.198.21.113:3000/api/suggestions/' + string)
    return this._http.get('http://localhost:3000/api/suggestions/' + string)
      .map((response: Response) => response.json());
  }
}
