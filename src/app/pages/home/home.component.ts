import { Component, OnInit } from '@angular/core';
import { ShowdataComponent } from '../howitworks/showdata/showdata.component';
import { Company } from '../../company.model';
import { CompanyresultsComponent } from '../howitworks/companyresults/companyresults.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  company: Company
  showResults: boolean = false;
  constructor() { }
scroll(x,y){
window.scrollTo(x,y);
}

scrollahead(){
  $('demo').animate({scrollTop:50+'px'},600);

}
  renderCompanyReceived(company: Company) {
    this.company = company;
    $('html,body').animate({ scrollTop: 600 + "px" }, 600);
    this.showResults = true;
  }

  ngOnInit() {

  }

}
