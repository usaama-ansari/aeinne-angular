import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomecontentComponent } from './welcomecontent.component';

describe('WelcomecontentComponent', () => {
  let component: WelcomecontentComponent;
  let fixture: ComponentFixture<WelcomecontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomecontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomecontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
