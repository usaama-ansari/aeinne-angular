import { Component, OnInit, OnChanges, HostListener } from '@angular/core';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {

  bgPosition: string;
  scrollValue:number;
  innerWidth: number;
  offset: number;
  constructor() { }
  ngAfterViewInit() {
       window.scrollTo(0, 0);
   }
      
  ngOnInit() {
    if(window.innerWidth < 780)
    this.offset = 0;
    else
      this.offset = -200;
  }
  
@HostListener('window:scroll', ['$event'])
//@HostListener('window:resize', ['$event'])
onWindowScroll(event)
{ 

   this.scrollValue = this.offset+window.scrollY/1.5;
   console.log(window.scrollY);
   this.bgPosition =  this.scrollValue.toString()+'px';
   console.log(this.bgPosition);
   }

}
