
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ElementRef,
  ViewChild,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { CompanyService } from '../../../company.service';
import { Company } from '../../../company.model';


@Component({
  selector: 'app-showdata',
  templateUrl: './showdata.component.html',
  styleUrls: ['./showdata.component.css'],
  providers: [CompanyService]
})
export class ShowdataComponent implements OnInit {
  @Output() companySend = new EventEmitter<object>();
  @ViewChild('iagree') iagree: ElementRef;
  @ViewChild('iagreePara') iagreePara: ElementRef;  
  @ViewChild('companyInput') companyInput: ElementRef
  string = "";
  valueset = false;
  company = Company;
  iagreeParaRef = this.iagreePara;
  loading = false;
  disbleState = true;
  sugName = [];
  typed: string = '';

  constructor(private _companyService: CompanyService) { }
  checkCompliance(inputValue : HTMLInputElement) {
    this.loading = true;
    console.log('check compliance executed');    
    this._companyService.checkCompliance(this.typed.replace(/[`~!@#$%^*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, ''))
      .subscribe((company) => {
        this.company = company;
        this.companySend.emit( this.company);
        this.loading = false;
        inputValue.value = '';
      });
    this.valueset = true;
  }

  getSugesstions(event: any) {
    document.getElementById('datalist').innerHTML = "";
    if (this.typed === "") { console.log('null string'); }
    else {
      console.log('typed=' + this.typed);
      console.log('event=' + event.key);
      var suggArray = [];
      this._companyService.getSuggestions(this.typed.replace(/[`~!@#$%^*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')).subscribe((company) => {
        this.sugName = company;
        for (var item in company) {
          suggArray[item] = company[item].company_name;
          var option = document.createElement('option');
          option.value = company[item].company_name;
          document.getElementById('datalist').appendChild(option);
        }
        console.log(suggArray);
      });
    }
  }
  activate_input(companyInput: HTMLInputElement){
    if(this.disbleState){
     this.disbleState = false;
     
    }
    else{
      this.disbleState = true;
    }

  }
  ngOnInit() {
 
  }

}
