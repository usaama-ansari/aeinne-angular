import { Component, OnInit, Input } from '@angular/core';
import { Company } from '../../../company.model';

@Component({
  selector: 'app-companyresults',
  templateUrl: './companyresults.component.html',
  styleUrls: ['./companyresults.component.css']
})
export class CompanyresultsComponent implements OnInit {
  showResults: boolean = false;
  index=0;
  @Input() company :Company;
 
  constructor() { }

  ngOnInit() {
  }

}
