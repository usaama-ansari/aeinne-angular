import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { Company } from '../../company.model';
import { Routes } from '@angular/router';
import {  CompanyresultsComponent } from './companyresults/companyresults.component';

@Component({
  selector: 'app-howitworks',
  templateUrl: './howitworks.component.html',
  styleUrls: ['./howitworks.component.css']
})
export class HowitworksComponent implements OnInit {
showResults: boolean = false;
index=0;
company :Company;

indCompliant:boolean = true;

  constructor() { }
 @HostListener('window:scroll', ['$event'])
//@HostListener('window:resize', ['$event'])
onWindowScroll(event)
{ 
  if(window.scrollY >=350){
  console.log("executed"+window.scrollY);
  $('.evaluate-h1').addClass('animated fadeInUp')
  }
}
   
renderCompanyReceived(company: Company){
     this.company = company;  
     this.showResults = true;
      //this code is to navigate to a portin on page with smooth scrolling effect
      // the company results are rendered
      $('html,body').animate({ scrollTop: 1400 + "px" }, 600);
     this.index=0;
}
  ngOnInit() {
    
  }
}
