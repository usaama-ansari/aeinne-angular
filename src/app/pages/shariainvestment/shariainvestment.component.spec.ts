import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShariainvestmentComponent } from './shariainvestment.component';

describe('ShariainvestmentComponent', () => {
  let component: ShariainvestmentComponent;
  let fixture: ComponentFixture<ShariainvestmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShariainvestmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShariainvestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
