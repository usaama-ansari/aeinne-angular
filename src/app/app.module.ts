import { BrowserModule } from '@angular/platform-browser';
import * as $ from 'jquery';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/home/contact/contact.component';
import { ComplianceComponent } from './compliance/compliance.component';
import { HowitworksComponent } from './pages/howitworks/howitworks.component';
import { ShowdataComponent } from './pages/howitworks/showdata/showdata.component';
import { ContentcarouselComponent } from './pages/home/contentcarousel/contentcarousel.component';
import { WelcomecontentComponent } from './pages/home/welcomecontent/welcomecontent.component';
import { AboutusComponent } from './pages/aboutus/aboutus.component';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { ShariainvestmentComponent } from './pages/shariainvestment/shariainvestment.component';
import { CompanyresultsComponent } from './pages/howitworks/companyresults/companyresults.component';




const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'how-it-works', component: HowitworksComponent },
  { path: 'about-us', component: AboutusComponent },
  { path: 'shariya-investment', component: ShariainvestmentComponent }

]
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ContactComponent,
    ComplianceComponent,
    HowitworksComponent,
    ShowdataComponent,
    ContentcarouselComponent,
    WelcomecontentComponent,
    AboutusComponent,
    ShariainvestmentComponent,
    CompanyresultsComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2PageScrollModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
