import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { Company } from '../company.model';

@Component({
  selector: 'app-compliance',
  templateUrl: './compliance.component.html',
  styleUrls: ['./compliance.component.css'],
  providers: [CompanyService]
})
export class ComplianceComponent implements OnInit {
  string = "";
  valueset = false;
  company = Company;
  constructor(private _companyService: CompanyService) { }
  checkCompliance(name:string) {
    this._companyService.checkCompliance(name).subscribe((company) =>{this.company=company;});
    this.valueset = true;
  }
  ngOnInit() {

  }

}
