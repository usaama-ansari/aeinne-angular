import { Component, OnInit, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  scrollPos: boolean = false;
  constructor(private _router: Router, private _current_route: ActivatedRoute) { }

  ngOnInit() {

  }

  scroll(x, y) {
    window.scrollTo(x, y);
  }
  scrollToElement(element,current_route) {  //this code is to navigate to a portin on page with smooth scrolling effect
    var parent = document.getElementsByClassName('.parent-route');
    if (this._router.url === current_route) {
      var position = $('#' + element).offset().top - 50;
      console.log(position);
      $('html,body').animate({ scrollTop: position + "px" }, 800);
    }
    else {
      window.scrollTo(0,0);
      this._router.navigate([current_route]);
    }
  }

  @HostListener("window:scroll", ['$event'])
  onWindowScroll($event) {
    if (window.scrollY > 100) {
      this.scrollPos = true;
    }
    else {
      this.scrollPos = false;
    }
  }


}
